﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using QR.Ptis.SiriUtilities.SiriData;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Controllers
{
    /// <summary>
    /// The SiriServiceTypeConstraint class is a custom constraint resolver that 
    /// can be used to check incoming URIs to determine whether they contain a 
    /// valid SIRI service type.
    /// </summary>
    /// 
    /// <example>
    /// If [RoutePrefix("siri/{siriServiceType:SiriServiceTypeConstraint}")] is specified and the
    /// relative portion of the incoming URI is "siri/pt/..." or "siri/et/..." or 
    /// "siri/vm/..." then this constraint will return true.
    /// </example>
    public class SiriServiceTypeConstraint : IRouteConstraint
    {
        /// <summary>
        /// Implements the <see cref="IHttpRouteConstraint.Match"/> method. This method ensures that
        /// the <see cref="SiriWebController"/> class is constrained to processing
        /// URI requests containing a known SIRI service type.
        /// </summary>
        /// 
        /// <param name="request">
        /// The HTTP request received from webMethods.
        /// </param>
        /// 
        /// <param name="route">
        /// The portion of the URI containing the route information.
        /// </param>
        /// 
        /// <param name="parameterName">
        /// The name of the parameter that is to be checked (i.e. "siriServiceType").
        /// </param>
        /// 
        /// <param name="values">
        /// A dictionary that contains the parameter values extracted from the URI.
        /// </param>
        /// 
        /// <param name="routeDirection">
        /// An object that indicates whether the constraint check is being performed  
        /// when an incoming request is being handled or when a URL is being generated.
        /// </param>
        /// 
        /// <returns>
        /// Returns true if the route matches the route prefix template and contains one 
        /// of the SIRI service types (ie. PT, ET or VM), false otherwise.
        /// </returns>
        public bool Match(HttpContext request, IRouter route, string parameterName,
                          RouteValueDictionary values, RouteDirection routeDirection)
        {
            bool matchFound = false;
            object value;

            if (values.TryGetValue(parameterName, out value) && value != null)
            {
                if (value.GetType() == typeof(string))
                {
                    string candidateServiceType = (string)value;

                    // Try to match the parameter value against one of the SIRI service types. We
                    // ignore case on the parameter value, so "pt" and "PT" will both match
                    // to enumerated type SiriServiceType.PT.
                    matchFound = Enum.IsDefined(typeof(SiriServiceType), candidateServiceType.ToUpper());
                }
            }
            return matchFound;
        } // Match
    }
}
