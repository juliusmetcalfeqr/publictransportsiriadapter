﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QR.Ptis.SiriUtilities.SiriData;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Validation;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Controllers
{
    [ApiController]
    [Route("siri/{siriServiceType:SiriServiceTypeConstraint}/")]
    public class SiriProducerController : ControllerBase
    {
        private readonly SiriXmlValidator xmlValidator;

        public SiriProducerController(SiriXmlValidator XmlValidator)
        {
            xmlValidator = XmlValidator;
        }

        [Route("subscription_request")]
        [HttpPost]
        
        public async Task<IActionResult> ProcessSubscriptionRequest(SiriServiceType siriServiceType)
        {
            string siriXml = await new StreamReader(Request.Body, Encoding.UTF8).ReadToEndAsync();
            IActionResult result = Ok(); // Result returned by this function
            SiriDocumentType expectedDocumentType = SiriDocumentType.SubscriptionRequest;
            string errorMessage;
            
            // First make sure that the service is in a state where it
            // can start process messages.
            if (CanProcessRequests)
            {
                // Make sure the model data is valid.
                if (ModelState.IsValid)
                {
                    // Make sure the Xml data was actually provided.
                    if (!String.IsNullOrWhiteSpace(siriXml))
                    {
                        // Validate the incoming xml string
                        xmlValidator.ValidateSiriDocument(siriServiceType, expectedDocumentType, siriXml);
                    }
                }
            }
            return result;
        }

        [Route("subscription_terminate")]
        [HttpPost]
        public async Task<IActionResult> ProcesSubscriptionTerminate(SiriServiceType siriServiceType)
        {
            string siriXml = await new StreamReader(Request.Body, Encoding.UTF8).ReadToEndAsync();
            IActionResult result = Ok(); // Result returned by this function
            SiriDocumentType expectedDocumentType = SiriDocumentType.TerminateSubscriptionRequest;
            string errorMessage;
            // First make sure that the service is in a state where it
            // can start process messages.
            if (CanProcessRequests)
            {
                // Make sure the model data is valid.
                if (ModelState.IsValid)
                {
                    // Validate the incoming xml string
                    xmlValidator.ValidateSiriDocument(siriServiceType, expectedDocumentType, siriXml);
                }
            }
            return result;
        }

        [Route("data_ready")]
        [HttpPost]
        public async Task<IActionResult> ProcessDataReadyReply(SiriServiceType siriServiceType)
        {
            string siriXml = await new StreamReader(Request.Body, Encoding.UTF8).ReadToEndAsync();
            IActionResult result = Ok(); // Result returned by this function
            SiriDocumentType expectedDocumentType = SiriDocumentType.SubscriptionRequest;
            string errorMessage;
            // First make sure that the service is in a state where it
            // can start process messages.
            if (CanProcessRequests)
            {
                // Make sure the model data is valid.
                if (ModelState.IsValid)
                {
                    // Validate the incoming xml string
                    xmlValidator.ValidateSiriDocument(siriServiceType, expectedDocumentType, siriXml);
                }
            }
            return result;
        }

        [Route("data_supply")]
        [HttpPost]
        public async Task<IActionResult> ProcessDataSupplyRequest(SiriServiceType siriServiceType)
        {
            string siriXml = await new StreamReader(Request.Body, Encoding.UTF8).ReadToEndAsync();
            IActionResult result = Ok(); // Result returned by this function
            SiriDocumentType expectedDocumentType = SiriDocumentType.SubscriptionRequest;
            string errorMessage;
            // First make sure that the service is in a state where it
            // can start process messages.
            if (CanProcessRequests)
            {
                // Make sure the model data is valid.
                if (ModelState.IsValid)
                {
                    // Validate the incoming xml string
                    xmlValidator.ValidateSiriDocument(siriServiceType, expectedDocumentType, siriXml);
                }
            }
            return result;
        }

        [Route("check_status")]
        [HttpPost]
        public async Task<IActionResult> ProcessCheckStatus(SiriServiceType siriServiceType)
        {
            string siriXml = await new StreamReader(Request.Body, Encoding.UTF8).ReadToEndAsync();
            IActionResult result = Ok(); // Result returned by this function
            SiriDocumentType expectedDocumentType = SiriDocumentType.SubscriptionRequest;
            string errorMessage;
            // First make sure that the service is in a state where it
            // can start process messages.
            if (CanProcessRequests)
            {
                // Make sure the model data is valid.
                if (ModelState.IsValid)
                {
                    // Validate the incoming xml string
                    xmlValidator.ValidateSiriDocument(siriServiceType, expectedDocumentType, siriXml);
                }
            }
            return result;
        }

        /// <summary>
        /// Property that specifies if requests can be processed.
        /// </summary>
        protected bool CanProcessRequests
        {
            get
            {
                // The SiriProducerController can process requests if the service has been fully
                // initialised and the Message Broker is connected.
                return true;// _serviceHealth.ServiceIsInitialised && _serviceHealth.BrokerIsConnected;
            }
        } // CanProcessRequests
    }
}
