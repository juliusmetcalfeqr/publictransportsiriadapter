﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Controllers
{
    class HealthStatusController : ControllerBase
    {

        [HttpPost]
        public async Task<IActionResult> PlaceholderController()
        {
            return Ok();
        }
    }
}
