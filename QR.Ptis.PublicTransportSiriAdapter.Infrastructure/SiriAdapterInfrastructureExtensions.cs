﻿using System;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using QR.Extensions.Amqp;
using QR.Reflection;
using QR.Reactive;

using QR.ApplicationMediator;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageProcessor;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.Queues;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Validation;
using QR.Ptis.SiriUtilities.SiriData;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure
{
    /// <summary>
    /// Extension methods for setting up the infrastructure classes.
    /// </summary>
    static public class SiriAdapterInfrastructureExtensions
    {
        /// <summary>
        /// Extension method that sets up the host builder to build the infrastructure objects.
        /// </summary>
        ///
        /// <param name="builder">
        /// The host builder that is being used to build up the application.
        /// </param>
        ///
        /// <returns>
        /// The updated <see cref="IHostBuilder"/> object.
        /// </returns>
        public static IHostBuilder AddSiriAdapterInfrastructure(this IHostBuilder builder)
        {
            builder.AddSiriAdapterAmqp(typeof(SiriAdapterInfrastructureExtensions).Assembly)
                   .AddSiriAdapterHttp(typeof(SiriAdapterInfrastructureExtensions).Assembly)
                   .ConfigureServices((context, service) =>
                   {
                       service.AddSingleton<IMediator, Mediator>()
                            .AddSingleton<IAmqpSender, AmqpSender>()
                            .AddSingleton<SiriXmlValidator>()
                            .AddSingleton<SiriXmlParser>()
                            .AddSingleton<IReactiveSchedulerFactory, ReactiveSchedulerFactory>()
                            .AddHttpClient<SiriHttpClient>((sp, httpClient) =>
                            {
                                httpClient.BaseAddress = new Uri("http://localhost:80");
                                httpClient.Timeout = TimeSpan.FromSeconds(5);
                            });



                   });
            return builder;
        }

        /// <summary>
        /// Extension method for adding the AMQP functionality to the host builder.
        /// </summary>
        ///
        /// <param name="builder">
        /// The host builder that is being used to build up the application.
        /// </param>
        /// <param name="assembly">
        /// The assembly that will be searched to find message handler and queue objects.
        /// </param>
        ///
        /// <returns>
        /// The updated <see cref="IHostBuilder"/> object.
        /// </returns>
        internal static IHostBuilder AddSiriAdapterAmqp(this IHostBuilder builder, Assembly assembly)
        {
            builder.AddAmqp(assembly)
                   .AddAmqpPasswordDecryptor<AmqpPasswordDecryptor<SiriAdapterEncryption>>()
                   .ConfigureServices(service =>
                   {
                       service.AddSingleton<IAmqpSender, AmqpSender>()
                              .AddHostedService<CommunicationService>()
                              .AddMessageSenders(assembly);
                   });
            return builder;

        }

        /// <summary>
        /// Extension method for adding the Web Host functionality to the host builder.
        /// </summary>
        ///
        /// <param name="builder">
        /// The host builder that is being used to build up the application.
        /// </param>
        /// <param name="assembly">
        /// The assembly that will be searched to find message handler and queue objects.
        /// </param>
        ///
        /// <returns>
        /// The updated <see cref="IHostBuilder"/> object.
        /// </returns>
        internal static IHostBuilder AddSiriAdapterHttp(this IHostBuilder builder, Assembly assembly)
        {
            builder.ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<HttpExtensions>();

            });
            return builder;
        }

        /// <summary>
        /// Helper method that adds all the sender classes that are used to send message via AMQP when they receive
        /// a message from the mediator.
        /// </summary>
        ///
        /// <param name="services">
        /// The services collection that the message senders will be added to.
        /// </param>
        /// <param name="assembly">
        /// The assembly that contains the message senders.
        /// </param>
        ///
        /// <returns>
        /// The updated <see cref="IServiceCollection"/> object.
        /// </returns>
        private static IServiceCollection AddMessageSenders(this IServiceCollection services, Assembly assembly)
        {
            // The message senders all implement the IMessageSender interface so find all the classes that implement
            // this interface and add them to the services collection as part of the interface and for the specific
            // type.
            var messageSenderTypes = assembly.GetAllOfType<IMessageSender>();

            foreach (var messageSenderType in messageSenderTypes)
            {
                services.AddSingleton(messageSenderType);
                services.AddSingleton(provider => (IMessageSender)provider.GetRequiredService(messageSenderType));
            }

            return services;
        }

    }
}
