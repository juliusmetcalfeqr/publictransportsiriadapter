﻿using QR.ParameterCheck;
using QR.Reflection;
using System;
using System.Reflection;

namespace QR.Ptis.PublicTransportSiriAdapter
{
    /// <summary>
    /// Contains information about the application.
    /// </summary>
    public class AppInformation
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="AppInformation"/> class.
        /// </summary>
        ///
        /// <param name="identity">
        /// The identity of the application.
        /// </param>
        /// <param name="assembly">
        /// The primary application assembly which contains information about the application such as its title.
        /// </param>
        public AppInformation(string identity, Assembly assembly)
        {
            assembly.WithParamCheckNotNull(nameof(assembly));

            Identity = identity.WithParamCheckNotNullOrWhiteSpace(nameof(identity));

            AssemblyInfoDetail assemblyInfo = new AssemblyInfoDetail(assembly);
            Title = assemblyInfo.Title;
            Version = assemblyInfo.Version;
        }

        /// <summary>
        /// The identity of the application.
        /// </summary>
        public string Identity
        {
            get;
        }

        /// <summary>
        /// The title of the application.
        /// </summary>
        public string Title
        {
            get;
        }

        /// <summary>
        /// The version of the application.
        /// </summary>
        public Version Version
        {
            get;
        }
    }
}
