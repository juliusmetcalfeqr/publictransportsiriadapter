﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure
{
    /// <summary>
    /// Contains constants that define the channel names used by the mediator.
    /// </summary>
    class MediatorChannelName
    {
        /// <summary>
        /// The name of the mediator channel that is used for PT messages.
        /// </summary>
        public const string PtMessages = "PtMessages";
        public const string EtMessages = "EtMessages";
        public const string VmMessages = "VmMessages";

        public const string OutPtMessages = "OutPtMessages";
        public const string OutSiriMessages = "OutSiriMessages";
        public const string OutEtMessages = "OutEtMessages";
        public const string OutVmMessages = "OutVmMessages";
    }
}
