﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure
{
    public static class ServiceId
    {
        /// <summary>
        /// The service that handles PT messages.
        /// </summary>
        public const string PtService = "PtService";

        /// <summary>
        /// The service that handles ET messages.
        /// </summary>
        public const string EtService = "EtService";

        /// <summary>
        /// The service that handles VM messages.
        /// </summary>
        public const string VmService = "VmService";

        /// <summary>
        /// The service that handles Siri responses.
        /// </summary>
        public const string ResponseService = "ResponseService";

    }
}
