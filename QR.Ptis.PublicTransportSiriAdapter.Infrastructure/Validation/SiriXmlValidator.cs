﻿using System;
using System.Collections.Generic;
using System.Text;
using QR.ApplicationMediator;
using QR.Ptis.SiriUtilities.SiriData;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;
using System.IO;
using QR.ParameterCheck;
using QR.Ptis.SiriUtilities;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Validation
{
    public class SiriXmlValidator
    {
        private SiriXmlParser _siriXmlParser;
        private IMediator _mediator;

        public SiriXmlValidator(SiriXmlParser siriXmlParser, IMediator mediator)
        {
            _mediator = mediator;
            _siriXmlParser = siriXmlParser;
        }

        public async void ValidateSiriDocument(SiriServiceType siriServiceType, SiriDocumentType expectedDocumentType, string siriXml)
        {
            // siriXml has been checked already. May still have to check other two params
            Siri document = await _siriXmlParser.ValidateAndExtractAsync(siriXml);
            // With this succeeding, the document type is confirmed.
            SiriDocumentType actualDocumentType = expectedDocumentType;

            // Further checks are done here to ensure that the rest of the Siri object is valid.

            switch (siriServiceType)
            {
                case SiriServiceType.PT:
                    _mediator.Publish(new PtSiriDocumentNotification(actualDocumentType, document));
                    break;
                case SiriServiceType.ET:
                    _mediator.Publish(new EtSiriDocumentNotification(actualDocumentType, document));
                    break;
                case SiriServiceType.VM:
                    _mediator.Publish(new VmSiriDocumentNotification(actualDocumentType, document));
                    break;
                default:
                    throw new InvalidDataException();
            }
            
        }
        
    }
}
