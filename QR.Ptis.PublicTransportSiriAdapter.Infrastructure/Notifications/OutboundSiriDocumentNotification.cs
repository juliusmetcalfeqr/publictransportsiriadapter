﻿using QR.ApplicationMediator;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure;
using QR.Ptis.SiriMessages;
using QR.Ptis.SiriUtilities.SiriData;
using QR.Ptis.SiriUtilities;
using QR.Ptis.Messages;
using QR.So.Amqp.MessageBroker.Messages;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications
{
    class OutboundSiriDocumentNotification : MediatorMessage
    {
        public OutboundSiriDocumentNotification(SiriDocumentMsg message)
            : base(MediatorChannelName.OutSiriMessages)
        {
            Message = message;
        }
        /// <summary>
        /// The message object
        /// </summary>
        public SiriDocumentMsg Message { get; set; }

    }
}
