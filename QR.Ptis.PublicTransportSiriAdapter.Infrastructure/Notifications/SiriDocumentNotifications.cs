﻿using System;
using System.Collections.Generic;
using System.Text;
using QR.ApplicationMediator;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure;
using QR.Ptis.SiriUtilities.SiriData;
using QR.Ptis.SiriUtilities;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications
{
    public class PtSiriDocumentNotification : MediatorMessage
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="PtSiriDocumentNotification"/> class.
        /// </summary>
        ///
        /// <param name="siriDocument">
        /// The Siri document object
        /// </param>
        /// <param name="docType">
        /// The Siri document type
        /// </param>
        public PtSiriDocumentNotification(SiriDocumentType docType, Siri siriDocument)
            : base(MediatorChannelName.PtMessages)
        {
            SiriDocument = siriDocument;
            DocType = docType;
        }
        /// <summary>
        /// The Siri document object
        /// </summary>
        public Siri SiriDocument { get; set; }
        /// <summary>
        /// The Siri document type
        /// </summary>
        public SiriDocumentType DocType { get; set; }
    }
    public class EtSiriDocumentNotification : MediatorMessage
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="EtSiriDocumentNotification"/> class.
        /// </summary>
        ///
        /// <param name="siriDocument">
        /// The Siri document object
        /// </param>
        /// <param name="docType">
        /// The Siri document type
        /// </param>
        public EtSiriDocumentNotification(SiriDocumentType docType, Siri siriDocument)
            : base(MediatorChannelName.EtMessages)
        {
            SiriDocument = siriDocument;
            DocType = docType;
        }
        /// <summary>
        /// The Siri document object
        /// </summary>
        public Siri SiriDocument { get; set; }
        /// <summary>
        /// The Siri document type
        /// </summary>
        public SiriDocumentType DocType { get; set; }

    }
    public class VmSiriDocumentNotification : MediatorMessage
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="VmSiriDocumentNotification"/> class.
        /// </summary>
        ///
        /// <param name="siriDocument">
        /// The Siri document object
        /// </param>
        /// <param name="docType">
        /// The Siri document type
        /// </param>
        public VmSiriDocumentNotification(SiriDocumentType docType, Siri siriDocument)
            : base(MediatorChannelName.VmMessages)
        {
            SiriDocument = siriDocument;
            DocType = docType;
        }
        /// <summary>
        /// The Siri document object
        /// </summary>
        public Siri SiriDocument { get; set; }
        /// <summary>
        /// The Siri document type
        /// </summary>
        public SiriDocumentType DocType { get; set; }

    }
}
