﻿using QR.Extensions.Amqp;
using QR.ParameterCheck;
using QR.So.Amqp.MessageBroker.Messages;
using System;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication
{
    /// <summary>
    /// Responsible for sending PTIS messages via AMQP. It ensures that certain conditions within the message are met
    /// such as setting the sender property.
    /// </summary>
    class AmqpSender : IAmqpSender
    {
        private readonly IAmqpClient _amqpClient;
        private readonly string _senderId;

        /// <summary>
        /// Initialises a new instance of the <see cref="AmqpSender"/> class.
        /// </summary>
        ///
        /// <param name="amqpClient">
        /// The AMQP client that will perform the actual send operation.
        /// </param>
        /// <param name="appInfo">
        /// Information about the application.
        /// </param>
        /// <param name="requestTracker">
        /// Used to track RPC requests that are sent via AMQP.
        /// </param>
        public AmqpSender(IAmqpClient amqpClient, AppInformation appInfo)
        {
            _amqpClient = amqpClient.WithParamCheckNotNull(nameof(amqpClient));
            _senderId = appInfo.WithParamCheckNotNull(nameof(appInfo)).Identity;
            // FIXME: Use PT, ET or VM instead of Guid
            ReplyToKey = $"PublicTransportSiriAdapter.ReplyTo.{appInfo.Identity}.{Guid.NewGuid()}";
        }


        /// <summary>
        /// Sends a message to the AMQP message broker.
        /// </summary>
        ///
        /// <param name="service">
        /// The service that the message is being sent to. This will be used to determine which connection to use.
        /// </param>
        /// <param name="message">
        /// The message to send.
        /// </param>
        /// <param name="persistent">
        /// True if the message shall continue to be in the queue even if the connection is lost.
        /// </param>
        /// <param name="correlationId">
        /// A unique identifier that will be used to match responses to a request when implementing an RPC interface.
        /// </param>
        public void SendMessage(string service,
                                BrokerMessageBase message,
                                bool persistent = false,
                                Guid correlationId = default)
        {
            // Make sure the message sender is assigned to the message.
            AssignMessageSender(message);

            // Send the message either with or without a correlation identifier depending on whether or not a
            // correlation identifier was provided.
            if (correlationId != Guid.Empty)
            {
                _amqpClient.SendMessage(service, message, persistent: persistent,
                                        correlationId: correlationId.ToString());
            }
            else
            {
                _amqpClient.SendMessage(service, message, persistent: persistent);
            }

            // Log the message that was sent for diagnostic purposes.
            AmqpMessageLogger.LogAmqpMessage(message, MessageDirection.Sent, service: service,
                                             correlationId: correlationId.ToString());
        }

        /// <summary>
        /// Helper method for ensuring that the sender property within a message is set.
        /// </summary>
        ///
        /// <param name="message">
        /// The message whose sender property should be set.
        /// </param>
        public void AssignMessageSender(BrokerMessageBase message)
        {
            if (message is ISenderBrokerMessage senderMessage)
            {
                if (senderMessage.Sender == null)
                {
                    senderMessage.Sender = _senderId;
                }
            }
        }


        /// <summary>
        /// The reply to routing key that should be used for all responses.
        /// </summary>
        public string ReplyToKey
        {
            get;
        }
    }
}
