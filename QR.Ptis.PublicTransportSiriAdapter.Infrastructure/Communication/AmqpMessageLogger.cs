﻿using NLog;
using NLog.Fluent;
using QR.ParameterCheck;
using QR.So.Amqp.MessageBroker.Messages;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication
{
    /// <summary>
    /// Class for logging AMQP messages to a NLog log file.
    /// </summary>
    static class AmqpMessageLogger
    {
        private static readonly ILogger _logger = LogManager.GetLogger("AmqpMessagesLog");


        /// <summary>
        /// Logs the details of an AMQP message that has been received.
        /// </summary>
        ///
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="direction">
        /// Whether the message was received or sent by this application.
        /// </param>
        /// <param name="service">
        /// The service identifier that specifies where the message is being sent when sending a message.
        /// </param>
        /// <param name="correlationId">
        /// The correlation ID supplied with the message.
        /// </param>
        /// <param name="replyToKey">
        /// The reply to key supplied with the message.
        /// </param>
        public static void LogAmqpMessage(BrokerMessageBase message,
                                          MessageDirection direction,
                                          string service = null,
                                          string correlationId = null,
                                          string replyToKey = null)
        {
            message.WithParamCheckNotNull(nameof(message));

            var logBuilder = _logger.Debug()
                                    .Property("Direction", direction);

            if (service != null)
            {
                logBuilder.Property("Service", service);
            }

            if (correlationId != null)
            {
                logBuilder.Property("CorrelationId", correlationId);
            }

            if (replyToKey != null)
            {
                logBuilder.Property("ReplyToKey", replyToKey);
            }

            logBuilder.Property("AmqpMessage", message)
                      .Write();
        }
    }
}
