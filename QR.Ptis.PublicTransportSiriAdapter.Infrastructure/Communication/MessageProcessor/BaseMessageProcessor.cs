﻿using NLog;
using System;
using System.Threading;
using System.Threading.Tasks;
using QR.Extensions.Amqp;
using QR.So.Amqp.MessageBroker;
using QR.So.Amqp.MessageBroker.Messages;
using QR.ParameterCheck;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageProcessor
{
    public abstract class BaseMessageProcessor<T> : IAmqpMessageProcessor where T : BrokerMessageBase, new()
    {
        private readonly ILogger _log;

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseMessageProcessor{T}"/> class.
        /// </summary>
        ///
        public BaseMessageProcessor(ILogger log)
        {
            _log = log.WithParamCheckNotNull(nameof(ILogger));
        }

        /// <summary>
        /// Processes the message.
        /// </summary>
        ///
        /// <param name="messageDetails">
        /// Details of the message that was received.
        /// </param>
        /// <param name="cancelToken">
        /// A cancellation token that signals if the message processing should be cancelled.
        /// </param>
        ///
        /// <returns>
        /// This is an asynchronous method so it returns a <see cref="Task"/> object.
        /// </returns>
        public async Task HandleMessage(BrokerMessageDetails messageDetails, CancellationToken cancelToken)
        {
            T message = ReadMessage(messageDetails);

            if (message != null)
            {
                _log.Info("[BaseMessageProcessor: HandleMessage] Valid message for processing!");
                await ProcessMessage(messageDetails, message);
            }
        }

        /// <summary>
        /// Extracts the message from the message details.
        /// </summary>
        ///
        /// <param name="messageDetails">
        /// The details of the message that was received.
        /// </param>
        ///
        /// <returns>
        /// The extracted message if it is valid or null if the message is invalid.
        /// </returns>
        private T ReadMessage(BrokerMessageDetails messageDetails)
        {
            T message = null;

            try
            {
                message = messageDetails.ReadMessage<T>();
            }
            catch (NotSupportedException versionError)
            {
                _log.Error("[BaseMessageProcessor: ReadMessage] Invalid version");
            }


            return message;
        }

        /// <summary>
        /// Processes the message after it has been decoded.
        /// </summary>
        /// 
        /// <param name="messageDetails">
        /// The details of the message that was received.
        /// </param>
        /// <param name="message">
        /// The decoded message.
        /// </param>
        /// <param name="cancelToken">
        /// A cancellation token that can be used to cancel the processing of the message.
        /// </param>
        ///
        /// <returns>
        /// This will be an asynchronous method so it will return a <see cref="Task"/> object.
        /// </returns>
        protected abstract Task ProcessMessage(BrokerMessageDetails messageDetails, T message);

    }
}
