﻿using NLog;
using System;
using System.Threading.Tasks;
using QR.ApplicationMediator;
using QR.Extensions.Amqp;
using QR.ParameterCheck;
using QR.So.Amqp.MessageBroker;
using QR.Ptis.SiriMessages;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageProcessor
{
    [AmqpMessageProcessorFilter(MessageType = nameof(SiriDocumentMsg))]
    public class ResponseDocumentProcessor : BaseMessageProcessor<SiriDocumentMsg>
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        private readonly IMediator _mediator;
        public ResponseDocumentProcessor(IMediator mediator)
            : base(_log)
        {
            _mediator = mediator.WithParamCheckNotNull(nameof(mediator));
        }

        /// <summary>
        /// Processes the message.
        /// </summary>
        ///
        /// <param name="messageDetails">
        /// The details of the message that was received.
        /// </param>
        /// <param name="message">
        /// The decoded message containing the status details.
        /// </param>
        /// <returns></returns>
        protected override Task ProcessMessage(BrokerMessageDetails messageDetails, SiriDocumentMsg message)
        {
            _log.Debug("Processing Response Message");
            // This message is a specific type, so we need to handle it specifically here, instead of 
            // passing the responsibility to another part of the application.
            try
            {
                _log.Debug("Message version: {0}, doc type: {0}", message.Version, message.DocType.ToString());
                _mediator.Publish(new OutboundSiriDocumentNotification(message));
            }
            catch (Exception error)
            {

            }
            return Task.CompletedTask;
        }
    }
}
