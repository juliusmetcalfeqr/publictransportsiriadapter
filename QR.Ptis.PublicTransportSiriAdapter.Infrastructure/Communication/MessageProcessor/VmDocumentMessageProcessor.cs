﻿using NLog;
using System;
using System.Threading.Tasks;
using QR.ApplicationMediator;
using QR.Extensions.Amqp;
using QR.ParameterCheck;
using QR.So.Amqp.MessageBroker;
using QR.Ptis.SiriMessages;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;
using QR.Ptis.SiriUtilities;
using QR.Ptis.SiriUtilities.SiriData;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageProcessor
{
    [AmqpMessageProcessorFilter(MessageType = nameof(VmDocumentMsg))]
    public class VmDocumentMessageProcessor: BaseMessageProcessor<VmDocumentMsg>
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        private readonly IMediator _mediator;
        public VmDocumentMessageProcessor(IMediator mediator)
            : base(_log)
        {
            _mediator = mediator.WithParamCheckNotNull(nameof(mediator));
        }
        /// <summary>
        /// Processes the message.
        /// </summary>
        ///
        /// <param name="messageDetails">
        /// The details of the message that was received.
        /// </param>
        /// <param name="message">
        /// The decoded message containing the status details.
        /// </param>
        /// <returns></returns>
        protected override Task ProcessMessage(BrokerMessageDetails messageDetails, VmDocumentMsg message)
        {
            _log.Debug("Processing VM Message");
            // This message is a specific type, so we need to handle it specifically here, instead of 
            // passing the responsibility to another part of the application.
            try
            {
                message.DocType = SiriDocumentType.SubscriptionRequest;
                _log.Debug("Message version: {0}, doc type: {0}", message.Version, message.DocType.ToString());
                _mediator.Publish(new OutboundSiriDocumentNotification(message));
            }
            catch (Exception error)
            {

            }
            return Task.CompletedTask;
        }
    }
}
