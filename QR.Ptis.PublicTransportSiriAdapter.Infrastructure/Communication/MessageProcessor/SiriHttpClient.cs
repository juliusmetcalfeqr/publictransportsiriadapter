﻿using NLog;
using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using System.Xml.Serialization;
using QR.ParameterCheck;
using QR.ApplicationMediator;
using QR.Ptis.SiriUtilities;
using QR.Ptis.SiriUtilities.SiriData;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageProcessor
{
    class SiriHttpClient : IDisposable
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        private readonly HttpClient _httpClient;
        private IDisposable _subscription;
        private bool _isDisposed;
        public SiriHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient.WithParamCheckNotNull(nameof(httpClient));
        }
        public void Register(IMediator mediator, IScheduler scheduler, CancellationToken cancelToken)
        {
            mediator.WithParamCheckNotNull(nameof(mediator));
            scheduler.WithParamCheckNotNull(nameof(scheduler));

            /// Subscribe to the message from the mediator that will trigger the http transmission.
            _subscription = mediator.CreateSubscription<OutboundSiriDocumentNotification>(MediatorChannelName.OutSiriMessages)
                                    .ObserveOn(scheduler)
                                    .WithHandler(CreateItemAsync);
        }

        /// <summary>
        /// Disposes of the resources used by this class.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Performs the actual disposal of resources used by this class.
        /// </summary>
        ///
        /// <param name="disposing">
        /// True if this instance of the class is being disposed and false if it is being finalised.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _subscription?.Dispose();
                }

                _isDisposed = true;
            }
        }

        public async Task CreateItemAsync(OutboundSiriDocumentNotification notification)
        {
            XmlSerializer s = new XmlSerializer(typeof(Siri));
            Siri siriRequest = notification.Message.SiriDocument;
            Encoding siriEncoding = new UTF8Encoding(false);
            var todoItem = new StringContent(SiriHelper.GetEncodedString(siriRequest, siriEncoding));

            var task2 = Task.Run(async () => {
                _log.Debug("{0}:\n{1}", notification.Message.DocType.ToString(), await todoItem.ReadAsStringAsync());
                return Task.CompletedTask;
            });

            var task1 = _httpClient.PostAsync("http://localhost:9111/siri", todoItem);
            task2.Wait();
            var httpResponse = await task1;
            httpResponse.EnsureSuccessStatusCode();
        }
    }
}
