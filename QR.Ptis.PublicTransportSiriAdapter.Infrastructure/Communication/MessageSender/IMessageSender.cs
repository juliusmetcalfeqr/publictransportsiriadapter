﻿using QR.ApplicationMediator;
using System.Reactive.Concurrency;
using System.Threading;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender
{
    /// <summary>
    /// Defines the interface that must be implemented by all classes that process a mediator message and send another
    /// message via AMQP.
    /// </summary>
    interface IMessageSender
    {
        /// <summary>
        /// Registers the message sender with the mediator so that it can process the required message.
        /// </summary>
        ///
        /// <param name="mediator">
        /// The mediator that will retrieve the message to be processed.
        /// </param>
        /// <param name="scheduler">
        /// A reactive extensions schedule that the message should be processed on.
        /// </param>
        /// <param name="amqpSender">
        /// The object that can be used to send messages via the AMQP message broker.
        /// </param>
        /// <param name="cancelToken">
        /// A cancellation token that can cancel the registration.
        /// </param>
        void Register(IMediator mediator, IScheduler scheduler, IAmqpSender amqpSender, CancellationToken cancelToken);
    }
}
