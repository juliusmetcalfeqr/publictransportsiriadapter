﻿using NLog;
using System;
using System.Threading.Tasks;
using QR.ParameterCheck;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;
using QR.Ptis.SiriUtilities;
using QR.Ptis.SiriMessages;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender
{
    class EtMessageSender : MessageSender<EtSiriDocumentNotification>
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        private readonly string _identity;
        /// <summary>
        /// Initialises a new instance of the <see cref="EtMessageSender"/> class.
        /// </summary>
        ///
        /// <param name="appInfo">
        /// Information about the application.
        /// </param>
        public EtMessageSender(AppInformation appInfo)
            : base(MediatorChannelName.EtMessages)
        {
            _identity = appInfo.WithParamCheckNotNull(nameof(appInfo)).Identity;
        }

        /// <summary>
        /// Processes the mediator message by sending an ET message.
        /// </summary>
        ///
        /// <param name="etNotification">
        /// The mediator message that contains the current status of the application.
        /// </param>
        ///
        /// <returns>
        /// A completed task object.
        /// </returns>
        protected override Task Handle(EtSiriDocumentNotification etNotification)
        {
            etNotification.SiriDocument.WithParamCheckNotNull<Siri>(nameof(Siri));

            try
            {
                EtDocumentMsg message = new EtDocumentMsg(etNotification.DocType, etNotification.SiriDocument)
                {
                    RouteKey = $"{SiriMessagesRouteKeys.ProcessorEtPrefix}"
                };

                AmqpSender.SendMessage(ServiceId.EtService, message);
                _log.Debug("Sent ET Message to {0}.", message.RouteKey);
            }
            catch (Exception error)
            {
                _log.Error(error, "An error occurred while trying to send an ET message.");
            }
            
            return Task.CompletedTask;
        }
    }
}
