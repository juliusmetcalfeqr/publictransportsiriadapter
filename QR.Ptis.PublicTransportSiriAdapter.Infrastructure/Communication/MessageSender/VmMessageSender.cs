﻿using NLog;
using System;
using System.Threading.Tasks;
using QR.ParameterCheck;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;
using QR.Ptis.SiriUtilities;
using QR.Ptis.SiriMessages;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender
{
    class VmMessageSender : MessageSender<VmSiriDocumentNotification>
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        private readonly string _identity;
        /// <summary>
        /// Initialises a new instance of the <see cref="VmMessageSender"/> class.
        /// </summary>
        ///
        /// <param name="appInfo">
        /// Information about the application.
        /// </param>
        public VmMessageSender(AppInformation appInfo)
            : base(MediatorChannelName.VmMessages)
        {
            _identity = appInfo.WithParamCheckNotNull(nameof(appInfo)).Identity;
        }

        /// <summary>
        /// Processes the mediator message by sending a VM message.
        /// </summary>
        ///
        /// <param name="vmNotification">
        /// The mediator message that contains the current status of the application.
        /// </param>
        ///
        /// <returns>
        /// A completed task object.
        /// </returns>
        protected override Task Handle(VmSiriDocumentNotification vmNotification)
        {
            vmNotification.SiriDocument.WithParamCheckNotNull<Siri>(nameof(Siri));

            try
            {
                VmDocumentMsg message = new VmDocumentMsg(vmNotification.DocType, vmNotification.SiriDocument)
                {
                    RouteKey = $"{SiriMessagesRouteKeys.ProcessorVmPrefix}"
                };

                AmqpSender.SendMessage(ServiceId.VmService, message);
                _log.Debug("Sent VM Message to {0}.", message.RouteKey);
            }
            catch (Exception error)
            {
                _log.Error(error, "An error occurred while trying to send a VM message.");
            }
            
            return Task.CompletedTask;
        }
    }
}
