﻿using QR.ApplicationMediator;
using QR.ParameterCheck;
using System;
using System.Reactive.Concurrency;
using System.Threading;
using System.Threading.Tasks;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender
{
    /// <summary>
    /// Base class for a class that handles a mediator message and sends a message via AMQP when the message does not
    /// require a return value.
    /// </summary>
    ///
    /// <typeparam name="T">
    /// The type of mediator message that will be handled.
    /// </typeparam>
    abstract class MessageSender<T> : IMessageSender, IDisposable where T : MediatorMessage
    {
        private readonly string _channel;
        private IDisposable _subscription;
        private bool _isDisposed;

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageSender{T}"/> class.
        /// </summary>
        ///
        /// <param name="channel">
        /// The name of the mediator channel that the message will be sent on.
        /// </param>
        public MessageSender(string channel)
        {
            _channel = channel.WithParamCheckNotNullOrWhiteSpace(nameof(channel));
        }


        /// <summary>
        /// Disposes of the resources used by this class.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Performs the actual disposal of resources used by this class.
        /// </summary>
        ///
        /// <param name="disposing">
        /// True if this instance of the class is being disposed and false if it is being finalised.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _subscription?.Dispose();
                }

                _isDisposed = true;
            }
        }



        /// <summary>
        /// Registers the message sender with the mediator so that it will receive the required messages.
        /// </summary>
        ///
        /// <param name="mediator">
        /// The mediator that will retrieve the required messages.
        /// </param>
        /// <param name="scheduler">
        /// A scheduler that the messages will be observed on.
        /// </param>
        /// <param name="amqpSender">
        /// The object that can be used to send messages via the AMQP message broker.
        /// </param>
        /// <param name="cancelToken">
        /// A cancellation token that can be used to cancel the registration.
        /// </param>
        public virtual void Register(IMediator mediator,
                                     IScheduler scheduler,
                                     IAmqpSender amqpSender,
                                     CancellationToken cancelToken)
        {
            mediator.WithParamCheckNotNull(nameof(mediator));
            scheduler.WithParamCheckNotNull(nameof(scheduler));

            /// Assign the sender to a member variable.
            AmqpSender = amqpSender.WithParamCheckNotNull(nameof(amqpSender));

            /// Subscribe to the message from the mediator that will trigger the message sending.
            _subscription = mediator.CreateSubscription<T>(_channel)
                                    .ObserveOn(scheduler)
                                    .WithHandler(Handle);
        }


        /// <summary>
        /// This method must be implemented in the derived class to handle the mediator message.
        /// </summary>
        ///
        /// <param name="message">
        /// The message that should be sent.
        /// </param>
        ///
        /// <returns>
        /// This is an asynchronous method so it returns a <see cref="Task"/> object.
        /// </returns>
        protected abstract Task Handle(T message);


        /// <summary>
        /// The object that can be used to send messages via the AMQP message broker.
        /// </summary>
        public IAmqpSender AmqpSender
        {
            get;
            private set;
        }
    }
}
