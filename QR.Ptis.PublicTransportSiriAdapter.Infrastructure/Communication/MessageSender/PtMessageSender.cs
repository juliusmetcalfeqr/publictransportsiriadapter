﻿using NLog;
using System;
using System.Threading.Tasks;
using QR.ParameterCheck;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Notifications;
using QR.Ptis.SiriMessages;
using QR.Ptis.SiriUtilities;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender
{
    class PtMessageSender : MessageSender<PtSiriDocumentNotification>
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        private readonly string _identity;

        /// <summary>
        /// Initialises a new instance of the <see cref="PtMessageSender"/> class.
        /// </summary>
        ///
        /// <param name="appInfo">
        /// Information about the application.
        /// </param>
        public PtMessageSender(AppInformation appInfo)
            : base(MediatorChannelName.PtMessages)
        {
            _identity = appInfo.WithParamCheckNotNull(nameof(appInfo)).Identity;
        }

        /// <summary>
        /// Processes the mediator message by sending a PT message.
        /// </summary>
        ///
        /// <param name="ptNotification">
        /// The mediator message that contains the current status of the application.
        /// </param>
        ///
        /// <returns>
        /// A completed task object.
        /// </returns>
        protected override Task Handle(PtSiriDocumentNotification ptNotification)
        {
            ptNotification.SiriDocument.WithParamCheckNotNull<Siri>(nameof(Siri));

            try
            {
                PtDocumentMsg message = new PtDocumentMsg(ptNotification.DocType, ptNotification.SiriDocument)
                {
                    RouteKey = $"{SiriMessagesRouteKeys.ProcessorPtPrefix}"
                };

                AmqpSender.SendMessage(ServiceId.PtService, message);
                _log.Debug("Sent PT Message to {0}.", message.RouteKey);
            }
            catch (Exception error)
            {
                _log.Error(error, "An error occurred while trying to send a PT message.");
            }
            
            return Task.CompletedTask;
        }
    }
}
