﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication
{
    public static class SiriProcessorRouteKeys
    {
        public const string PtPrefix = "QR.PublicTransportSiriProcessor.PT";
        public const string EtPrefix = "QR.PublicTransportSiriProcessor.ET";
        public const string VmPrefix = "QR.PublicTransportSiriProcessor.VM";
    }
}
