﻿using QR.So.Amqp.MessageBroker.Messages;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication
{
    /// <summary>
    /// Defines the interface for the class that will send messages via AMQP.
    /// </summary>
    public interface IAmqpSender
    {
        /// <summary>
        /// Sends a message to the AMQP message broker.
        /// </summary>
        ///
        /// <param name="service">
        /// The service that the message is being sent to. This will be used to determine which connection to use.
        /// </param>
        /// <param name="message">
        /// The message to send.
        /// </param>
        /// <param name="persistent">
        /// True if the message shall continue to be in the queue even if the connection is lost.
        /// </param>
        /// <param name="correlationId">
        /// A unique identifier that will be used to match responses to a request when implementing an RPC interface.
        /// </param>
        void SendMessage(string service,
                         BrokerMessageBase message,
                         bool persistent = false,
                         Guid correlationId = default);

        /// <summary>
        /// The reply to routing key that should be used for all responses.
        /// </summary>
        string ReplyToKey
        {
            get;
        }
    }
}
