﻿using QR.Extensions.Amqp;
using QR.ParameterCheck;
using QR.Ptis.SiriMessages;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.Queues
{
    /// <summary>
    /// The AMQP queue that receives PT messages related to this Siri Adapter.
    /// </summary>
    class PtMessageQueue : AmqpQueue
    {
        private readonly string _replyToKey;
        private readonly string _queueName;


        /// <summary>
        /// Initialises a new instance of the <see cref="PtMessageQueue"/> class.
        /// </summary>
        /// 
        /// <param name="amqpSender">
        /// The AMQP sender object that contains the reply to key to subscribe to receive responses to requests.
        /// </param>
        /// <param name="appInfo">
        /// Information about the application.
        /// </param>
        public PtMessageQueue(IAmqpSender amqpSender, AppInformation appInfo)
            : base(ServiceId.PtService, exclusive: true)
        {
            _replyToKey = amqpSender.WithParamCheckNotNull(nameof(amqpSender)).ReplyToKey;
            _queueName = $"QR.PublicTransportSiriAdapter.{appInfo.Identity}.{Guid.NewGuid()}";
        }


        /// <summary>
        /// Initialises the queue.
        /// </summary>
        ///
        /// <param name="cancelToken">
        /// A cancellation token that can be used to cancel the initialisation.
        /// </param>
        ///
        /// <returns>
        /// This is an asynchronous method so it returns a <see cref="Task"/> object.
        /// </returns>
        protected override Task InitialiseQueue(CancellationToken cancelToken)
        {
            Subscribe(_replyToKey);
            Subscribe($"{SiriMessagesRouteKeys.AdapterPtPrefix}");
            StartListening();
            return Task.CompletedTask;
        }


        /// <summary>
        /// Returns the name to use for the queue.
        /// </summary>
        ///
        /// <returns>
        /// The queue name.
        /// </returns>
        protected override string GenerateQueueName()
        {
            return _queueName;
        }
    }
}
