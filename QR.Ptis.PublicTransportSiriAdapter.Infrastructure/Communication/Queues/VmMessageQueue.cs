﻿using QR.Extensions.Amqp;
using QR.ParameterCheck;
using QR.Ptis.SiriMessages;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.Queues
{
    /// <summary>
    /// The AMQP queue that receives VM messages related to this Siri Processor.
    /// </summary>
    class VmMessageQueue : AmqpQueue
    {
        private readonly string _replyToKey;
        private readonly string _queueName;


        /// <summary>
        /// Initialises a new instance of the <see cref="VmMessageQueue"/> class.
        /// </summary>
        /// 
        /// <param name="amqpSender">
        /// The AMQP sender object that contains the reply to key to subscribe to receive responses to requests.
        /// </param>
        /// <param name="appInfo">
        /// Information about the application.
        /// </param>
        public VmMessageQueue(IAmqpSender amqpSender, AppInformation appInfo)
            : base(ServiceId.VmService, exclusive: true)
        {
            _replyToKey = amqpSender.WithParamCheckNotNull(nameof(amqpSender)).ReplyToKey;
            _queueName = $"QR.PublicTransportSiriAdapter.{appInfo.Identity}.{Guid.NewGuid()}";
        }


        /// <summary>
        /// Initialises the queue.
        /// </summary>
        ///
        /// <param name="cancelToken">
        /// A cancellation token that can be used to cancel the initialisation.
        /// </param>
        ///
        /// <returns>
        /// This is an asynchronous method so it returns a <see cref="Task"/> object.
        /// </returns>
        protected override Task InitialiseQueue(CancellationToken cancelToken)
        {
            Subscribe(_replyToKey);
            Subscribe($"{SiriMessagesRouteKeys.AdapterVmPrefix}");
            StartListening();
            return Task.CompletedTask;
        }


        /// <summary>
        /// Returns the name to use for the queue.
        /// </summary>
        ///
        /// <returns>
        /// The queue name.
        /// </returns>
        protected override string GenerateQueueName()
        {
            return _queueName;
        }
    }
}
