﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using Microsoft.Extensions.Hosting;
using QR.Reactive;
using QR.ParameterCheck;
using QR.ApplicationMediator;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageSender;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication.MessageProcessor;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication
{
    /// <summary>
    /// Background service responsible for the overall management of the communication functionality.
    /// </summary>
    class CommunicationService : BackgroundService
    {
        private readonly IMediator _mediator;
        private readonly IAmqpSender _amqpSender;
        private readonly List<IMessageSender> _messageSenders;
        private readonly IScheduler _scheduler;
        private readonly SiriHttpClient _httpClient;

        private bool _isDisposed;

        /// <summary>
        /// Initialises a new instance of the <see cref="CommunicationService"/> class.
        /// </summary>
        ///
        /// <param name="mediator">
        /// The mediator that is used to pass messages between different parts of the application.
        /// </param>
        /// <param name="amqpSender">
        /// The object that can be used to send messages via the AMQP message broker.
        /// </param>
        /// <param name="messageSenders">
        /// All the message sender classes that are responsible for sending AMQP messages when specific Mediator
        /// messages are published.
        /// </param>
        /// <param name="schedulerFactory">
        /// Factory class for creating reactive extension scheduler objects.
        /// </param>
        public CommunicationService(IMediator mediator, IAmqpSender amqpSender, IEnumerable<IMessageSender> messageSenders, IReactiveSchedulerFactory schedulerFactory, SiriHttpClient httpClient)
        {
            messageSenders.WithParamCheckNotNull(nameof(messageSenders));

            _mediator = mediator.WithParamCheckNotNull(nameof(mediator));
            _amqpSender = amqpSender.WithParamCheckNotNull(nameof(amqpSender));
            _scheduler = schedulerFactory.WithParamCheckNotNull(nameof(schedulerFactory))
                                         .CreateEventLoopScheduler();
            _httpClient = httpClient.WithParamCheckNotNull(nameof(httpClient));
            _messageSenders = new List<IMessageSender>(messageSenders);
        }


        /// <summary>
        /// Disposes of the resources used by this class.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            Dispose(true);
        }


        /// <summary>
        /// Performs the actual disposal of resources used by this class.
        /// </summary>
        ///
        /// <param name="disposing">
        /// True if this instance of the class is being disposed and false if it is being finalised.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    (_scheduler as IDisposable)?.Dispose();
                }

                _isDisposed = true;
            }
        }


        /// <summary>
        /// Called when the communication functionality should be set up.
        /// </summary>
        ///
        /// <param name="stoppingToken">
        /// A cancellation token that will be set to a cancelled state when the application is shutting down.
        /// </param>
        ///
        /// <returns>
        /// A completed task object.
        /// </returns>
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            foreach (var messageSender in _messageSenders)
            {
                messageSender.Register(_mediator, _scheduler, _amqpSender, stoppingToken);
            }
            _httpClient.Register(_mediator, _scheduler, stoppingToken);
            return Task.CompletedTask;
        }
    }
}
