﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure.Communication
{
    /// <summary>
    /// Specifies if a AMQP message was received or sent.
    /// </summary>
    enum MessageDirection
    {
        /// <summary>
        /// The message was received by the application.
        /// </summary>
        Received,

        /// <summary>
        /// The message was sent by the application.
        /// </summary>
        Sent
    }
}
