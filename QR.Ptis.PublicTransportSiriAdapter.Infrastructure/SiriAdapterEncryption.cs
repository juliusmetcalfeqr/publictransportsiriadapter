﻿using QR.Encryption;

namespace QR.Ptis.PublicTransportSiriAdapter.Infrastructure
{
    /// <summary>
    /// Encrypts and decrypts string used within the SIRI Adapter.
    /// </summary>
    public class SiriAdapterEncryption : EncryptionBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SiriAdapterEncryption"/> class.
        /// </summary>
        public SiriAdapterEncryption()
            : base(new byte[] { 10, 144, 213, 79, 192, 4, 54, 127, 211, 145, 25, 91, 8, 128, 2, 76, 83, 39, 74, 90, 36, 48, 3, 18 },
                   "bz274$2!PqH=@)a/")
        {
        }
    }
}