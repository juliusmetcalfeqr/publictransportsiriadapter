﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace QR.Ptis.PublicTransportSiriAdapter
{
    /// <summary>
    /// Helper class for determining the full path and file name of the configuration files.
    /// </summary>
    public class ConfigurationFileNames
    {
        private const string AmqpSettingsFileName = "AmqpSettings.json";
        private const string HttpSettingsFileName = "HttpSettings.json";
        private const string AppSettingsFileName = 
            "appSettings.json";

        private readonly IConfigurationRoot _configRoot;
        private string _path;

        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationFileNames"/> class.
        /// </summary>
        ///
        /// <param name="args">
        /// The command line arguments passed to the application.
        /// </param>
        public ConfigurationFileNames(string[] args)
        {
            _configRoot = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appSettings.json", optional: true)
                .AddEnvironmentVariables(AppStartUp.EnvironmentPrefix)
                .AddCommandLine(args)
                .Build();
        }

        /// <summary>
        /// Gets the path where the configuration files will be stored.
        /// </summary>
        /// <returns></returns>
        private string GetConfigPath()
        {
            // If the path has not been initialised then set it to the application data path contained in the CAP
            // connection configuration settings. Otherwise set it to a zero length string which effecitively makes
            // the path the current directory.
            if (_path == null)
            {
                var section = _configRoot.GetSection("CapConnection:ApplicationDataPath");

                if (section.Exists())
                {
                    _path = section.Value;
                }
                else
                {
                    _path = String.Empty;
                }
            }

            return _path;
        }

        /// <summary>
        /// The file name of the Siri Adapter settings file.
        /// </summary>
        public string AppSettings
        {
            get
            {
                return Path.Combine(GetConfigPath(), AppSettingsFileName);
            }
        }

        /// <summary>
        /// The file name of the AMQP settings file.
        /// </summary>
        public string AmqpSettings
        {
            get
            {
                return Path.Combine(GetConfigPath(), AmqpSettingsFileName);
            }
        }

        /// <summary>
        /// The file name of the HTTP settings file.
        /// </summary>
        public string HttpSettings
        {
            get
            {
                return Path.Combine(GetConfigPath(), HttpSettingsFileName);
            }
        }
    }
}
