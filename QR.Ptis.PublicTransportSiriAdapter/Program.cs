using System;
using NLog;
using QR.ApplicationStartUp;

namespace QR.Ptis.PublicTransportSiriAdapter
{
    public class Program
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            AppStartUp appStartUp = new AppStartUp();
            try
            {
                appStartUp.AddIdentityProvider(new JsonIdentityProvider("appSettings.json", "Identity"))
                            .AddIdentityProvider(new EnvironmentIdentityProvider(AppStartUp.EnvironmentPrefix))
                            .Start(args);
            }
            catch (Exception error)
            {
                _log.Error(error, "A start up error has occurred.");
            }
        }
    }
}
