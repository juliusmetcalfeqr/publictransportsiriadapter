using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using QR.ApplicationStartUp;
using QR.Extensions.HealthProcessor;
using QR.Extensions.Hosting;
using QR.Extensions.Keyboard;
using QR.Extensions.Keyboard.Health;
using QR.Extensions.Logging;
using McMaster.Extensions.CommandLineUtils;
using QR.Ptis.PublicTransportSiriAdapter.Infrastructure;

namespace QR.Ptis.PublicTransportSiriAdapter
{
    public class AppStartUp: ApplicationStartupBase
    {
        /// <summary>
        /// Prefix to use for any environment variables used by this application.
        /// </summary>
        public const string EnvironmentPrefix = "PublicTransportSiriAdapter_";

        /// <summary>
        /// Configures the application command line object to handle the command line parameters.
        /// </summary>
        ///
        /// <param name="commandLineApp">
        /// The command line application object that will handle the command line parameters.
        /// </param>
        protected override void ConfigureCommandLineSettings(CommandLineApplication commandLineApp)
        {
            commandLineApp.AddVersion<AppStartUp>()
                          .AddConsole()
                          .AddIdentityOverride();
        }

        protected override async Task StartApplication(string[] args)
        {
            ConfigurationFileNames configFileNames = new ConfigurationFileNames(args);

            bool runAsService = !(Debugger.IsAttached || CommandLineApp.IsConsoleCommandLineSpecified());

            if (Identity == null)
            {
                throw new InvalidOperationException("The application identity has not been set.");
            } 
            else
            {
                Console.WriteLine("Identity of application is {0}", Identity);
            }

            var appInfo = new AppInformation(Identity, typeof(AppStartUp).Assembly);
            var host = new HostBuilder()
                .ConfigureServices((context, service) =>
                {
                    service.AddSingleton<AppInformation>(new AppInformation(Identity, typeof(AppStartUp).Assembly));
                })
                //.ConfigureDefaultGenericHost<AppStartUp>(CommandLineApp, environmentPrefix: EnvironmentPrefix)
                .AddNLog()
                .AddLogDeletion()
                .AddDefaultKeyboardHandlers()
                .ConfigureAppConfiguration(config =>
                {
                    // Configure the additional settings files needed by the application other than the default
                    // appSettings.json file. Also add environment variables and command line parameters so that they
                    // are given a higher priority.
                    config.AddJsonFile(configFileNames.AmqpSettings, true, true)
                          .AddJsonFile(configFileNames.AppSettings, true, true)
                          .AddJsonFile(configFileNames.HttpSettings, true, true)
                          .AddEnvironmentVariables()
                          .AddEnvironmentVariables(EnvironmentPrefix)
                          .AddCommandLine(args);
                })
                .AddSiriAdapterInfrastructure();

            // Run the application as a service if it should run as a service otherwise run it as a console application.
            if (runAsService)
            {
                //await host.RunAsServiceAsync().ConfigureAwait(false);
            }
            else
            {
                await host.RunConsoleAsync().ConfigureAwait(false);
            }
        }
    }
}
